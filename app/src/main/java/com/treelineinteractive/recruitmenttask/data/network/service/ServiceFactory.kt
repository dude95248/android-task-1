package com.treelineinteractive.recruitmenttask.data.network.service

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.treelineinteractive.recruitmenttask.BuildConfig
import com.treelineinteractive.recruitmenttask.data.network.ApiConst
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceFactory {
    inline fun <reified T> createService(): T =
        retrofit(baseUrl = ApiConst.BASE_URL).create(T::class.java)

    /**
     * HEADERS or BODY have potential to leak sensitive information. Default level is None.
     * This data should be logged in non-production environment if needed.
     */
    private val interceptor = HttpLoggingInterceptor()

    init {
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        }
    }

    fun retrofit(baseUrl: String): Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(okHttp())
        .build()

    private fun okHttp(): OkHttpClient {
        val builder = OkHttpClient.Builder().addInterceptor(interceptor)
        return builder.build()
    }
}
