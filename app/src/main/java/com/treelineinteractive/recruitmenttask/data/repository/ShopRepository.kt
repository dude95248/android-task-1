package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.service.ServiceFactory
import com.treelineinteractive.recruitmenttask.data.network.service.ShopService

class ShopRepository {
    private val shopService = ServiceFactory.createService<ShopService>()
    private val productsLocalCache = ProductsLocalCache()

    suspend fun getProducts(): List<ProductItem> {
        val response = shopService.getInventory()

        if(response.isSuccessful && response.body().isNullOrEmpty().not()){
            val products = response.body()!!
            productsLocalCache.saveProducts(products)
            return products
        }

        // When getting inventory from server fails, try cache data
        return productsLocalCache.getProducts()
    }
}

class ProductsLocalCache {
    private val savedProducts = mutableListOf<ProductItem>()

    fun saveProducts(products: List<ProductItem>) = savedProducts.addAll(products)

    fun getProducts() = savedProducts
}