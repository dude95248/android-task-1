package com.treelineinteractive.recruitmenttask.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.ui.view.ProductItemView

class ShopItemsAdapter : RecyclerView.Adapter<ShopItemsAdapter.ProductItemViewHolder>() {
    private var currentAdapterItems: ArrayList<ProductItem> = arrayListOf()

    fun setList(newShopItems: List<ProductItem>) {
        currentAdapterItems.clear()
        currentAdapterItems.addAll(newShopItems)
        notifyDataSetChanged() //todo fix this by implementing DiffUtils
    }

    override fun getItemCount() = currentAdapterItems.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductItemViewHolder {
        return ProductItemViewHolder(ProductItemView(parent.context))
    }

    override fun onBindViewHolder(holder: ProductItemViewHolder, position: Int) =
        holder.bind(currentAdapterItems[position])

    class ProductItemViewHolder(private val productItemView: ProductItemView) :
        RecyclerView.ViewHolder(productItemView.binding.root) {

        //todo implement functionality to store data in case of app died
        fun bind(productItem: ProductItem) = with(productItemView) {
            setProductItem(productItem)

            binding.addItemButton.setOnClickListener {
                increaseAmount()
            }
            binding.removeItemButton.setOnClickListener {
                decreaseAmount()
            }
        }
    }
}