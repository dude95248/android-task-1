package com.treelineinteractive.recruitmenttask.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding
import com.treelineinteractive.recruitmenttask.ui.adapter.ShopItemsAdapter
import com.treelineinteractive.recruitmenttask.ui.utils.observer
import com.treelineinteractive.recruitmenttask.ui.utils.viewBinding
import com.treelineinteractive.recruitmenttask.ui.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val mainViewModel = MainViewModel()
    private lateinit var shopItemsAdapter: ShopItemsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        bindListeners()
        bindMainObserver()
        bindAdapterForShopItemList()
    }

    private fun bindAdapterForShopItemList() {
        shopItemsAdapter = ShopItemsAdapter()
        binding.itemsLayoutRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = shopItemsAdapter
        }
    }

    /**
     * Place for setting up listeners related to this view.
     */
    private fun bindListeners() {
        binding.retryButton.setOnClickListener {
            mainViewModel.loadProducts()
        }
    }

    private fun bindMainObserver() {
        mainViewModel.stateLiveData.observe { state ->
            setCurrentStateInView(state)

            if (state.isSuccess) {
                loadProductItemsToView(state.items)
            }
        }
    }

    private fun setCurrentStateInView(state: MainViewModel.MainViewState) {
        binding.progressBar.isVisible = state.isLoading
        binding.errorLayout.isVisible = state.error != null
        binding.itemsLayoutRecyclerView.isVisible = state.isSuccess
        binding.errorLabel.text = state.error
    }

    private fun loadProductItemsToView(items: List<ProductItem>) {
        shopItemsAdapter.setList(items)
    }

    private fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }
}