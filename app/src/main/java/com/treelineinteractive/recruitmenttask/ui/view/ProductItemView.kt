package com.treelineinteractive.recruitmenttask.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.R
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding

class ProductItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.materialCardViewStyle
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private var _binding: ViewProductItemBinding? =
        ViewProductItemBinding.inflate(LayoutInflater.from(context), this, false)
    val binding get() = _binding!!

    private var quantityAvailableToday: Int = 0
    private var currentAmount: Int = 0
    private var currentSold: Int = 0

    init {
        updateSoldView()
    }

    fun setProductItem(productItem: ProductItem) {
        quantityAvailableToday = productItem.available
        currentAmount = productItem.available

        binding.nameLabel.text = productItem.title
        binding.descriptionLabel.text = productItem.description
        updateAmountView(productItem.available.toString())
    }

    private fun updateAmountView(amount: String) {
        binding.availableLabelValue.text = amount
    }
    private fun updateSoldView(){
        binding.soldLabelValue.text = currentSold.toString()
    }

    fun increaseAmount() {
        if (quantityAvailableToday > currentAmount){
            currentAmount += 1
            currentSold -= 1
            updateAmountView(currentAmount.toString())
            updateSoldView()
        }
    }

    fun decreaseAmount() {
        if (currentAmount > 0) {
            currentAmount -= 1
            currentSold += 1
            updateAmountView(currentAmount.toString())
            updateSoldView()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        _binding = null
    }
}